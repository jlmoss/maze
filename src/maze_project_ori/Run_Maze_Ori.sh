# This script calls ./visualizer and ./hw0 simultaneously.
# All arguments to this script will be passed onto ./hw0

trap 'kill %1' SIGINT
  ./maze_project_ori  resources/maze_project_ori/world.urdf resources/maze_project_ori/Puma.urdf puma resources/maze_project_ori/ball.urdf Ballbot & ./visualizer  resources/maze_project_ori/world.urdf resources/maze_project_ori/Puma.urdf puma resources/maze_project_ori/ball.urdf Ballbot  & ./simulator resources/maze_project_ori/world.urdf resources/maze_project_ori/Puma.urdf puma  resources/maze_project_ori/ball.urdf Ballbot "$@"

