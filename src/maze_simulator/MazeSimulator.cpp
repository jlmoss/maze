#include "MazeSimulator.h"

#include <iostream>
#include <fstream>
#include "puma/RedisDriver.h"


#include <signal.h>
static volatile bool g_runloop = true;
void stop(int) { g_runloop = false; }
static RedisClient *g_redis_client = nullptr;

static volatile bool running_simulation = true;

using namespace std;

// Declare control variables for sending to puma redis
Eigen::VectorXd Kp(Puma::DOF);
Eigen::VectorXd Kv(Puma::DOF);
Eigen::VectorXd x_des_opp_(Puma::SIZE_OP_SPACE_TASK);



// Return true if any elements in the Eigen::MatrixXd are NaN
template<typename Derived>
static inline bool isnan(const Eigen::MatrixBase<Derived>& x) {
	return (x.array() != x.array()).any();
    
    
    if (g_redis_client != nullptr)
        g_redis_client->set(Puma::KEY_CONTROL_MODE, "BREAK");
    cout << "Sending BREAK." << endl;
    exit(1);
}





static void setCtrlCHandler(void (*userCallback)(int)) {
    struct sigaction sigIntHandler;
    sigIntHandler.sa_handler = userCallback;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);
}




/**
 * MazeProject::readRedisValues()
 * ------------------------------
 * Retrieve all read keys from Redis.
 */
void MazeSimulator::readRedisValues() {
    if (running_simulation){
        // read from Redis current sensor values
        redis_client_.getEigenMatrixDerivedString(JOINT_ANGLES_KEY, robot->_q);
        redis_client_.getEigenMatrixDerivedString(JOINT_VELOCITIES_KEY, robot->_dq);
        
        //for ball
        redis_client_.getEigenMatrixDerivedString(BALL_JOINT_ANGLES_KEY, ball->_q);
        redis_client_.getEigenMatrixDerivedString(BALL_JOINT_VELOCITIES_KEY, ball->_dq);
        redis_client_.getEigenMatrixDerivedString(BALL_POS_DES_KEY, ball_pos_des_);
        
        // Get current simulation timestamp from Redis
        redis_client_.getCommandIs(TIMESTAMP_KEY, redis_buf_);
        t_curr_ = stod(redis_buf_);
        
        // Read in KP and KV from Redis (can be changed on the fly in Redis)
        redis_client_.getCommandIs(KP_POSITION_KEY, redis_buf_);
        kp_pos_ = stoi(redis_buf_);
        redis_client_.getCommandIs(KV_POSITION_KEY, redis_buf_);
        kv_pos_ = stoi(redis_buf_);
        redis_client_.getCommandIs(KP_ORIENTATION_KEY, redis_buf_);
        kp_ori_ = stoi(redis_buf_);
        redis_client_.getCommandIs(KV_ORIENTATION_KEY, redis_buf_);
        kv_ori_ = stoi(redis_buf_);
        redis_client_.getCommandIs(KP_JOINT_KEY, redis_buf_);
        kp_joint_ = stoi(redis_buf_);
        redis_client_.getCommandIs(KV_JOINT_KEY, redis_buf_);
        kv_joint_ = stoi(redis_buf_);
    }
    else{
        // Read from Redis current sensor values and update the model
        robot->_q = redis_client_.getEigenMatrixString(Puma::KEY_JOINT_POSITIONS);
        robot->_dq = redis_client_.getEigenMatrixString(Puma::KEY_JOINT_VELOCITIES);
    }

}

/**
 * MazeProject::writeRedisValues()
 * -------------------------------
 * Send all write keys to Redis.
 */
void MazeSimulator::writeRedisValues() {
 
        // Send end effector position and desired position
        redis_client_.setEigenMatrixDerivedString(EE_POSITION_KEY, x_);
        //redis_client_.setEigenMatrixDerivedString(EE_POSITION_DESIRED_KEY, x_des_);
        //redis_client_.setEigenMatrixDerivedString(BALL_POS_DES_KEY, x_des_);
        
        // Send torques
        redis_client_.setEigenMatrixDerivedString(JOINT_TORQUES_COMMANDED_KEY, command_torques_);
   
        // Send command
        redis_client_.setEigenMatrixString(Puma::KEY_COMMAND_DATA, x_des_);
        // Send desired position for visualization
        redis_client_.setEigenMatrixString(Puma::KEY_EE_POS + "_des", ee_pos_des_);

 
	
   
}

/**
 * MazeProject::updateModel()
 * --------------------------
 * Update the robot model and all the relevant member variables.
 */
void MazeSimulator::updateModel() {
 
       	// Update the model
        robot->updateModel();
        
        ball->updateModel();
        // Forward kinematics
        robot->position(x_, "end-effector", Eigen::Vector3d::Zero());
        robot->linearVelocity(dx_, "end-effector", Eigen::Vector3d::Zero());
        
        // Jacobians
        robot->Jv(Jv_, "end-effector", Eigen::Vector3d::Zero());
        robot->Jw(Jw_, "end-effector");
        J_task_<<Jv_,Jw_;

        robot->nullspaceMatrix(N_, Jv_);
        
        // Dynamics
        robot->taskInertiaMatrixWithPseudoInv(Lambda_x_, Jv_);
        robot->taskInertiaMatrixWithPseudoInv( Lambda_ori_ , J_task_);
        
        robot->rotation(ori_,"end-effector");
        
        robot->angularVelocity(omega_,"end-effector");
        
        
        
        robot->gravityVector(g_);



}

/**
 * MazeProject::computeJointSpaceControlTorques()
 * ----------------------------------------------
 * Controller to initialize robot to desired joint position.
 */
MazeSimulator::ControllerStatus MazeSimulator::computeJointSpaceControlTorques() {
	// Finish if the robot has converged to q_initial
	Eigen::VectorXd q_err = robot->_q - q_des_;
	Eigen::VectorXd dq_err = robot->_dq - dq_des_;

	if (q_err.norm() < kToleranceInitQ && dq_err.norm() < kToleranceInitDq) {
		return FINISHED;
	}

	// Compute torques
	Eigen::VectorXd ddq = -kp_joint_ * q_err - kv_joint_ * dq_err;
	command_torques_ = robot->_M * ddq + g_;
   
	return RUNNING;
}

/**
 * MazeProject::computeOperationalSpaceControlTorques()
 * ----------------------------------------------------
 * Controller to move end effector to desired position.
 */
MazeSimulator::ControllerStatus MazeSimulator::computeOperationalSpaceControlTorques() {
    
    // set position to be constant
    ee_pos_des_ << ee_pos_init_(0), ee_pos_init_(1), ee_pos_init_(2);
    
    x_des_=ee_pos_des_ ;

    // get ball position
    ball->position(ball_pos_, "link3",Eigen::Vector3d::Zero());
    ball->linearVelocity(ball_vel_, "link3",Eigen::Vector3d::Zero());

    //if (ball_pos_(0) - )


    // PD position control with velocity saturation
    //Eigen::Vector3d x_err = x_ - x_des_;
    // Eigen::Vector3d dx_err = dx_ - dx_des_;
    // Eigen::Vector3d ddx = -kp_pos_ * x_err - kv_pos_ * dx_err_;
    //dx_des_ = -(kp_pos_ / kv_pos_) * x_err;
    //double v = kMaxVelocity / dx_des_.norm();
    //if (v > 1) v = 1;
    //Eigen::Vector3d dx_err = dx_ - v * dx_des_;
    //Eigen::Vector3d ddx = -kv_pos_ * dx_err;
    
//    // Nullspace posture control and damping
//    Eigen::VectorXd q_err = robot->_q - q_des_;
//    Eigen::VectorXd dq_err = robot->_dq - dq_des_;
//    Eigen::VectorXd ddq = -kp_joint_ * q_err - kv_joint_ * dq_err;
    
    
            ori_des_ << 1,0, 0,
            0, 1,0,
            0, 0, 1;

            double epsilon = 0.04;
            if(abs(ball_pos_(1) - ball_pos_des_(1)) > epsilon){
                double thetax = -kp_theta_*(ball_pos_des_(1) - ball_pos_(1)) - kv_theta_*(ball_vel_(1));
                ori_des_ << 1, 0, 0, 
                0, cos(thetax), -sin(thetax),
                0, sin(thetax), cos(thetax);
                cout << ball_pos_(0) << " " <<  ball_pos_(1) << endl;
            } else if(abs(ball_pos_(0) - ball_pos_des_(0)) > epsilon){
                double thetay = kp_theta_*(ball_pos_des_(0) - ball_pos_(0)) + kv_theta_*(ball_vel_(0));
                ori_des_ << cos(thetay), 0, sin(thetay), 
                0, 1, 0,
                -sin(thetay), 0, cos(thetay);
                //cout << ball_pos_(0) << " " <<  ball_pos_(1) << endl;
            }
    
            cout << ball_pos_(0) << " " <<  ball_pos_(1) << endl;
           
            //cout << ball_pos_(0) << " " <<  ball_pos_(1) << endl;
    

    
            robot->orientationError(delta_phi,ori_des_,ori_);
    
             F_star_=kp_pos_*(x_des_-x_ ) - (kv_pos_ * dx_);
             M_star_= kp_ori_*(-delta_phi)- kv_ori_*omega_;
             F_orientation_<<F_star_, M_star_;
             F_orientation_= Lambda_ori_*F_orientation_;

    
            
            
            

    
    
    // Control torques
    //Eigen::Vector3d F_x = Lambda_x_ * (-kp_pos_ * x_err );
    //Eigen::VectorXd F_posture = robot->_M * ddq;
    
   
    
    
 
       
        command_torques_ = J_task_.transpose()*F_orientation_  + g_ ;
  
        // - convert rotation matrix to quaternion, ensure that quaternion has not flipped sign
        ee_ori_des_ = Eigen::Quaterniond(ori_des_);
        // Send command
        x_des_opp_ << ee_pos_des_, ee_ori_des_.w(), ee_ori_des_.x(), ee_ori_des_.y(), ee_ori_des_.z();
    

  

    
  
    
    
	return RUNNING;
}

/**
 * public MazeProject::initialize()
 * --------------------------------
 * Initialize timer and Redis client
 */
void MazeSimulator::initialize() {
	// Create a loop timer
	timer_.setLoopFrequency(kControlFreq);   // 1 KHz
	// timer.setThreadHighPriority();  // make timing more accurate. requires running executable as sudo.
	timer_.setCtrlCHandler(stop);    // exit while loop on ctrl-c
	timer_.initializeTimer(kInitializationPause); // 1 ms pause before starting loop

	// Start redis client
	// Make sure redis-server is running at localhost with default port 6379
    auto redis_client = RedisClient();
	redis_client_.serverIs(kRedisServerInfo);
    
    // Set Ctrl C handler
    setCtrlCHandler(stop);
    g_redis_client = &redis_client;
    
    /***** Hold *****/
    
//    cout << "HOLD" << endl;
//    if (!running_simulation){
//           redis_client.set(Puma::KEY_CONTROL_MODE, "JHOLD");
//    }
 

	// Set gains in Redis if not initialized
	//if (!redis_client_.getCommandIs(KP_POSITION_KEY)) {
		redis_buf_ = to_string(kp_pos_);
		redis_client_.setCommandIs(KP_POSITION_KEY, redis_buf_);
	//}
	//if (!redis_client_.getCommandIs(KV_POSITION_KEY)) {
		redis_buf_ = to_string(kv_pos_);
		redis_client_.setCommandIs(KV_POSITION_KEY, redis_buf_);
	//}
	//if (!redis_client_.getCommandIs(KP_ORIENTATION_KEY)) {
		redis_buf_ = to_string(kp_ori_);
		redis_client_.setCommandIs(KP_ORIENTATION_KEY, redis_buf_);
	//}
	//if (!redis_client_.getCommandIs(KV_ORIENTATION_KEY)) {
		redis_buf_ = to_string(kv_ori_);
		redis_client_.setCommandIs(KV_ORIENTATION_KEY, redis_buf_);
	//}
	//if (!redis_client_.getCommandIs(KP_JOINT_KEY)) {
		redis_buf_ = to_string(kp_joint_);
		redis_client_.setCommandIs(KP_JOINT_KEY, redis_buf_);
	//}
	//if (!redis_client_.getCommandIs(KV_JOINT_KEY)) {
		redis_buf_ = to_string(kv_joint_);
		redis_client_.setCommandIs(KV_JOINT_KEY, redis_buf_);
	//}
    

    //set up ball
    redis_client_.setEigenMatrixDerivedString(BALL_JOINT_ANGLES_KEY, ball_q_des_);
    redis_client_.setEigenMatrixDerivedString(BALL_JOINT_VELOCITIES_KEY, ball_dq_des_);
    //for Ball
    redis_client_.setEigenMatrixDerivedString(BALL_JOINT_TORQUES_COMMANDED_KEY, ball_command_torques_);
    redis_client_.setEigenMatrixDerivedString(BALL_POS_DES_KEY, ball_pos_des_);
//    ball->_q = ball_q_des_;
//    ball->_dq = ball_dq_des_;
    ball->updateModel();
    
}

/**
 * public MazeProject::runLoop()
 * -----------------------------
 * MazeProject state machine
 */


void MazeSimulator::runLoop() {
	while (g_runloop) {
		// Wait for next scheduled loop (controller must run at precise rate)
		timer_.waitForNextLoop();
		++controller_counter_;

		// Get latest sensor values from Redis and update robot model
		readRedisValues();
		updateModel();

        controller_state_ = OP_SPACE_POSITION_CONTROL;

		switch (controller_state_) {
			// Wait until valid sensor values have been published to Redis
			case REDIS_SYNCHRONIZATION:
                
				if (isnan(robot->_q) & isnan(ball->_q) ) continue;
				cout << "Redis synchronized. Switching to joint space controller." << endl;
				controller_state_ = JOINT_SPACE_INITIALIZATION;
                
                cout << "JGOTO" << endl;
                
              
                    // If lowering gains, set Kp first. If increasing, set Kv first.
                    Kp.fill(kp_joint_);
                    Kv.fill(kv_joint_);
                    
                    //set kp and kv
                    redis_client_.mset({
                        {Puma::KEY_CONTROL_MODE, "JGOTO"},
                        {Puma::KEY_COMMAND_DATA, RedisClient::encodeEigenMatrixString(q_des_)},
                        {Puma::KEY_KP, RedisClient::encodeEigenMatrixString(Kp)},
                        {Puma::KEY_KV, RedisClient::encodeEigenMatrixString(Kv)}
                    });
                
             
                
				break;

			// Initialize robot to default joint configuration
			case JOINT_SPACE_INITIALIZATION:
                
				if (computeJointSpaceControlTorques() == FINISHED) {
                    
					cout << "Joint position initialized. Switching to operational space controller." << endl;
                    cout << "GOTO" << endl;
                  
                        //set up ball
                        redis_client_.setEigenMatrixDerivedString(BALL_JOINT_ANGLES_KEY, ball_q_des_);
                        redis_client_.setEigenMatrixDerivedString(BALL_JOINT_VELOCITIES_KEY, ball_dq_des_);
                        //for Ball
                        redis_client_.setEigenMatrixDerivedString(BALL_JOINT_TORQUES_COMMANDED_KEY, ball_command_torques_);
                        //    ball->_q = ball_q_des_;
                        //    ball->_dq = ball_dq_des_;
                        ball->updateModel();
                    
                
                        Kp.fill(kp_pos_);
                        Kv.fill(kv_pos_);
                        // When setting new gains, must change control mode simultaneously.
                        x_des_opp_<< ee_pos_des_, ee_ori_des_.w(), ee_ori_des_.x(), ee_ori_des_.y(), ee_ori_des_.z();
                        redis_client_.mset({
                            {Puma::KEY_CONTROL_MODE, "GOTO"},
                            {Puma::KEY_COMMAND_DATA, RedisClient::encodeEigenMatrixString(x_des_opp_)},
                            {Puma::KEY_KP, RedisClient::encodeEigenMatrixString(Kp)},
                            {Puma::KEY_KV, RedisClient::encodeEigenMatrixString(Kv)}
                        });
                  

                    
					controller_state_ = MazeSimulator::OP_SPACE_POSITION_CONTROL;
				};
              

               

				break;

			// Control end effector to desired position
			case OP_SPACE_POSITION_CONTROL:
				computeOperationalSpaceControlTorques();
        
              
				break;

			// Invalid state. Zero torques and exit program.
			default:
				cout << "Invalid controller state. Stopping controller." << endl;
				g_runloop = false;
				command_torques_.setZero();
                ball_command_torques_.setZero();
				break;
		}

		// Check command torques before sending them
		if (isnan(command_torques_)) {
			cout << "NaN command torques. Sending zero torques to robot." << endl;
			command_torques_.setZero();
            ball_command_torques_.setZero();
		}

		// Send command torques
		writeRedisValues();
	}

    
        // Zero out torques before quitting
        command_torques_.setZero();
        ball_command_torques_.setZero();
        redis_client_.setEigenMatrixDerivedString(JOINT_TORQUES_COMMANDED_KEY, command_torques_);
        redis_client_.setEigenMatrixDerivedString(BALL_JOINT_TORQUES_COMMANDED_KEY, ball_command_torques_);
  
        //stop robot
        redis_client_.set(Puma::KEY_CONTROL_MODE, "BREAK");
    

}

int main(int argc, char** argv) {

	// Parse command line
	if (argc != 6) {
		cout << "Usage: maze_project <path-to-world.urdf> <path-to-robot.urdf> <robot-name> <path-to-ball.urdf> <ball_name>" << endl;
		exit(0);
	}
	// argument 0: executable name
	// argument 1: <path-to-world.urdf>
	string world_file(argv[1]);
	// argument 2: <path-to-robot.urdf>
	string robot_file(argv[2]);
	// argument 3: <robot-name>
	string robot_name(argv[3]);
    // argument 2: <path-to-ball.urdf>
    string ball_file(argv[4]);
    // argument 3: <ball-name>
    string ball_name(argv[5]);

	// Load robot
	cout << "Loading robot for Maze: " << robot_file << endl;
	auto robot = make_shared<Model::ModelInterface>(robot_file, Model::rbdl, Model::urdf, false);
	robot->updateModel();
    //Load Ball
    cout << "Loading ball: " << ball_file << endl;
    auto ball = make_shared<Model::ModelInterface>(ball_file, Model::rbdl, Model::urdf, false);
    ball->updateModel();

	// Start controller app
	cout << "Initializing app with " << robot_name << endl;

	MazeSimulator app(move(robot), robot_name, move(ball), ball_name);
	app.initialize();
	cout << "App initialized. Waiting for Redis synchronization." << endl;
	app.runLoop();

    
//    //for ball
//    cout << "Initializing app with " << ball_name << endl;

	return 0;
}


