# This script calls ./visualizer and ./hw0 simultaneously.
# All arguments to this script will be passed onto ./hw0

trap 'kill %1' SIGINT
./maze_simulator resources/maze_simulator/world.urdf resources/maze_simulator/Puma.urdf Puma resources/maze_simulator/ball.urdf Ballbot  & ./visualizer  resources/maze_simulator/world.urdf resources/maze_simulator/Puma.urdf Puma resources/maze_simulator/ball.urdf Ballbot  & ./simulator resources/maze_simulator/world.urdf resources/maze_simulator/Puma.urdf Puma  resources/maze_simulator/ball.urdf Ballbot "$@"

