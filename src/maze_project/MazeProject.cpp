#include "MazeProject.h"

#include <iostream>
#include <fstream>
#include "puma/RedisDriver.h"


#include <signal.h>
static volatile bool g_runloop = true;

static RedisClient *g_redis_client_ = nullptr;

//static volatile bool running_simulation = false;

using namespace std;

static  std::string robot_name = "";
static std::string world_file = "";
static std::string robot_file = "";
static std::string ball_name = "";
static std::string ball_file = "";



    

// Declare control variables for sending to puma redis
Eigen::VectorXd Kp(Puma::DOF);
Eigen::VectorXd Kv(Puma::DOF);
Eigen::VectorXd x_des_opp_(Puma::SIZE_OP_SPACE_TASK);

void stop(int) {
    g_runloop = false;
    
    if (g_redis_client_ != nullptr)
        g_redis_client_->set(Puma::KEY_CONTROL_MODE, "BREAK");
    cout << "Sending BREAK." << endl;
    exit(1);}


// Return true if any elements in the Eigen::MatrixXd are NaN
template<typename Derived>
static inline bool isnan(const Eigen::MatrixBase<Derived>& x) {
	return (x.array() != x.array()).any();
    

}


static void setCtrlCHandler(void (*userCallback)(int)) {
    struct sigaction sigIntHandler;
    sigIntHandler.sa_handler = userCallback;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);
}




/**
 * MazeProject::readRedisValues()
 * ------------------------------
 * Retrieve all read keys from Redis.
 */
void MazeProject::readRedisValues() {
    
        // read from Redis current sensor values
        //for ball
        redis_client_.getEigenMatrixDerivedString(BALL_JOINT_ANGLES_KEY, ball->_q);
        redis_client_.getEigenMatrixDerivedString(BALL_JOINT_VELOCITIES_KEY, ball->_dq);
    
//       redis_client_.getEigenMatrixDerivedString(BALL_POS_DES_KEY, ball_pos_des_);
//    
//        redis_client_.getEigenMatrixDerivedString(BALL_POS_KEY, ball_pos_);
//        redis_client_.getEigenMatrixDerivedString(BALL_VEL_KEY, ball_vel_);

    redis_client_.getCommandIs(BALL_POS_X_KEY, redis_buf_);
    ball_x_pos_ = stod(redis_buf_);

    redis_client_.getCommandIs(BALL_POS_Y_KEY, redis_buf_);
    ball_y_pos_ = stod(redis_buf_);

    redis_client_.getCommandIs(BALL_POS_DES_X_KEY, redis_buf_);
    ball_x_pos_des_ = stod(redis_buf_);

    redis_client_.getCommandIs(BALL_POS_DES_Y_KEY, redis_buf_);
    ball_y_pos_des_ = stod(redis_buf_);


    
        // Get current simulation timestamp from Redis
        redis_client_.getCommandIs(TIMESTAMP_KEY, redis_buf_);
        t_curr_ = stod(redis_buf_);
        t_curr_ = timer_.elapsedTime();
        
        // Read in KP and KV from Redis (can be12.4552 changed on the fly in Redis)
        redis_client_.getCommandIs(KP_POSITION_KEY, redis_buf_);
        kp_pos_ = stoi(redis_buf_);
        redis_client_.getCommandIs(KV_POSITION_KEY, redis_buf_);
        kv_pos_ = stoi(redis_buf_);
        redis_client_.getCommandIs(KP_ORIENTATION_KEY, redis_buf_);
        kp_ori_ = stoi(redis_buf_);
        redis_client_.getCommandIs(KV_ORIENTATION_KEY, redis_buf_);
        kv_ori_ = stoi(redis_buf_);
        redis_client_.getCommandIs(KP_JOINT_KEY, redis_buf_);
        kp_joint_ = stoi(redis_buf_);
        redis_client_.getCommandIs(KV_JOINT_KEY, redis_buf_);
        kv_joint_ = stoi(redis_buf_);
    
    redis_client_.getCommandIs(KP_THETA_KEY, redis_buf_);
    kp_theta_ = stod(redis_buf_);
    redis_client_.getCommandIs(KV_THETA_KEY, redis_buf_);
    kv_theta_ = stod(redis_buf_);

    redis_client_.getCommandIs(BALL_EPSILON_KEY, redis_buf_);
    epsilon = stod(redis_buf_);
    redis_client_.getCommandIs(BALL_FRAC_KEY, redis_buf_);
    ball_frac_ = stod(redis_buf_);
    
    
    //try catch to make sure key positions are init in redis
    try {
       robot->_q = redis_client_.getEigenMatrixString(Puma::KEY_JOINT_POSITIONS);
    } catch (const std::overflow_error& e) {
        // this executes if f() throws std::overflow_error (same type rule)
          std::cout << e.what(); // information from length_error printed
    } catch (const std::runtime_error& e) {
        // this executes if f() throws std::underflow_error (base class rule)
          std::cout << e.what(); // information from length_error printed
    } catch (const std::exception& e) {
        // this executes if f() throws std::logic_error (base class rule)
          std::cout << e.what(); // information from length_error printed
    } catch (...) {
        // this executes if f() throws std::string or int or any other unrelated type
          cout << "error pos" << endl; // information from length_error printed
    }
    try {
        robot->_dq = redis_client_.getEigenMatrixString(Puma::KEY_JOINT_VELOCITIES);
    } catch (const std::overflow_error& e) {
        // this executes if f() throws std::overflow_error (same type rule)
        std::cout << e.what(); // information from length_error printed
    } catch (const std::runtime_error& e) {
        // this executes if f() throws std::underflow_error (base class rule)
        std::cout << e.what(); // information from length_error printed
    } catch (const std::exception& e) {
        // this executes if f() throws std::logic_error (base class rule)
        std::cout << e.what(); // information from length_error printed
    } catch (...) {
        // this executes if f() throws std::string or int or any other unrelated type
        cout << "error vel" << endl; // information from length_error printed
    }
    

//        // Read from Redis current sensor values and update the model
//        robot->_q = redis_client_.getEigenMatrixString(Puma::KEY_JOINT_POSITIONS);
//        robot->_dq = redis_client_.getEigenMatrixString(Puma::KEY_JOINT_VELOCITIES);
//    

}

/**
 * MazeProject::writeRedisValues()
 * -------------------------------
 * Send all write keys to Redis.
 */
void MazeProject::writeRedisValues() {
 
        // Send end effector position and desired position
        //redis_client_.setEigenMatrixDerivedString(EE_POSITION_KEY, x_);
        //redis_client_.setEigenMatrixDerivedString(EE_POSITION_DESIRED_KEY, x_des_);
        
        // Send torques
        redis_client_.setEigenMatrixDerivedString(JOINT_TORQUES_COMMANDED_KEY, command_torques_);
   
    
        // Send desired position for visualization
       // redis_client_.setEigenMatrixString(Puma::KEY_EE_POS + "_des", x_des_opp_);

        // If lowering gains, set Kp first. If increasing, set Kv first.
        Kp.fill(kp_joint_);
        Kv.fill(kv_joint_);
    
    
    	switch (controller_state_) {
			
			case REDIS_SYNCHRONIZATION:
                
                
				break;

			// Initialize robot to default joint configuration
			case JOINT_SPACE_INITIALIZATION:
        			// Send command
       				 //redis_client_.setEigenMatrixString(Puma::KEY_COMMAND_DATA, q_des_);
                
                redis_client_.mset({
                    {Puma::KEY_CONTROL_MODE, "JGOTO"},
                    {Puma::KEY_COMMAND_DATA, RedisClient::encodeEigenMatrixString(q_des_)},
                    {Puma::KEY_KP, RedisClient::encodeEigenMatrixString(Kp)},
                    {Puma::KEY_KV, RedisClient::encodeEigenMatrixString(Kv)}
                });
             
				break;
			case OP_SPACE_POSITION_CONTROL:
       				 // Send command
        			//redis_client_.setEigenMatrixString(Puma::KEY_COMMAND_DATA, x_des_opp_);

                
                
                redis_client_.mset({
                    {Puma::KEY_CONTROL_MODE, "GOTO"},
                    {Puma::KEY_COMMAND_DATA, RedisClient::encodeEigenMatrixString(x_des_opp_)},
                    {Puma::KEY_KP, RedisClient::encodeEigenMatrixString(Kp)},
                    {Puma::KEY_KV, RedisClient::encodeEigenMatrixString(Kv)}
                });
              
				break;
			}

    
 			
	
   
}

/**
 * MazeProject::updateModel()
 * --------------------------
 * Update the robot model and all the relevant member variables.
 */
void MazeProject::updateModel() {
 
       	// Update the model
        robot->updateModel();
        
        ball->updateModel();
        // Forward kinematics
        robot->position(x_, "end-effector", Eigen::Vector3d::Zero());
        robot->linearVelocity(dx_, "end-effector", Eigen::Vector3d::Zero());
        
        // Jacobians
        robot->Jv(Jv_, "end-effector", Eigen::Vector3d::Zero());
        robot->Jw(Jw_, "end-effector");
        J_task_<<Jv_,Jw_;

        robot->nullspaceMatrix(N_, Jv_);
        
        // Dynamics
        robot->taskInertiaMatrixWithPseudoInv(Lambda_x_, Jv_);
        robot->taskInertiaMatrixWithPseudoInv( Lambda_ori_ , J_task_);
        
        robot->rotation(ori_,"end-effector");
        
        robot->angularVelocity(omega_,"end-effector");
        
        
        
        robot->gravityVector(g_);



}

/**
 * MazeProject::computeJointSpaceControlTorques()
 * ----------------------------------------------
 * Controller to initialize robot to desired joint position.
 */
MazeProject::ControllerStatus MazeProject::computeJointSpaceControlTorques() {
	// Finish if the robot has converged to q_initial
	Eigen::VectorXd q_err = robot->_q - q_des_;
	Eigen::VectorXd dq_err = robot->_dq - dq_des_;

	if (q_err.norm() < kToleranceInitQ && dq_err.norm() < kToleranceInitDq) {
                t_last_= t_curr_;
		return FINISHED;
		
	}

	// Compute torques
	Eigen::VectorXd ddq = -kp_joint_ * q_err - kv_joint_ * dq_err;
	command_torques_ = robot->_M * ddq + g_;
 
    return RUNNING;
}

/**
 * MazeProject::computeOperationalSpaceControlTorques()
 * ----------------------------------------------------
 * Controller to move end effector to desired position.
 */
MazeProject::ControllerStatus MazeProject::computeOperationalSpaceControlTorques() {
    
    // Create a circle trajectory
    ee_pos_des_ << ee_pos_init_(0), ee_pos_init_(1), ee_pos_init_(2);
    
    x_des_=ee_pos_des_ ;
    // PD position
    //Eigen::Vector3d x_err = x_ - x_des_;
    
    
//            ori_des_ << 0,1, 0,
//            0, 0,1,
//            0, 1, 0;
//
//    float a_ang  =  M_PI/30;
//        float b_ang  =  M_PI/30;
//        float y_ang  =0;
//             ori_des_<< cos(b_ang)*cos(y_ang),cos(y_ang)*sin(a_ang)*sin(b_ang)- cos(a_ang)*sin(y_ang), cos(a_ang)*cos(y_ang)*sin(b_ang)+ sin(a_ang)*sin(y_ang),
//             cos(b_ang)*sin(y_ang), cos(a_ang)*cos(y_ang) +sin(a_ang)*sin(b_ang)*sin(y_ang),-cos(y_ang)*cos(a_ang) +cos(a_ang)*sin(b_ang)*sin(y_ang),
//             -sin(b_ang), cos(b_ang)*sin(a_ang), cos(a_ang)*cos(b_ang);
//
    
//           ori_des_=ori_;
//    
//            robot->orientationError(delta_phi,Eigen::Quaterniond(ori_des_),Eigen::Quaterniond(ori_));
//    
//             F_star_=kp_pos_*(x_des_-x_ ) - (kv_pos_ * dx_);
//             M_star_= kp_ori_*(-delta_phi)- kv_ori_*omega_;
//             F_orientation_<<F_star_, M_star_;
//             F_orientation_= Lambda_ori_*F_orientation_;
    
//
//    
//
    //Jess Code/////////
    ori_des_ << 1,0, 0,
    0, 1,0,
    0, 0, 1;
    ori_x_des_ = ori_des_;
    ori_y_des_ = ori_des_;
    //ball_y_pos_ = .5;
    //set pos and des from redis
    ball_pos_ << ball_y_pos_,ball_x_pos_,0;
    ball_pos_des_ << ball_y_pos_des_,ball_x_pos_des_,0;	
 
   ball_vel_ = ball_frac_ *((ball_pos_- ball_pos_last_)/(t_curr_-t_last_)) + (1-ball_frac_)*ball_vel_;
ball_pos_last_ =ball_pos_;
    t_last_ = t_curr_;
   



    if(abs(ball_pos_(0) - ball_pos_des_(0)) > epsilon){
        double thetax = -kp_theta_*(ball_pos_des_(0) - ball_pos_(0)) - kv_theta_*(ball_vel_(0));
	thetax = min(0.6,max(-0.6,thetax)); 

        ori_x_des_ << 1, 0, 0,
        0, cos(thetax), -sin(thetax),
        0, sin(thetax), cos(thetax);
        cout << "thetaX " << thetax <<"BALL Velocity                " << ball_vel_.transpose()<<  endl;
//        cout << "ori_des_" << endl;
//
//        cout << ori_des_ << endl;
//        cout << "ori_" << endl;
//
//        cout << ori_ << endl;
        //cout << ball_pos_(0) << " " <<  ball_pos_(1) << endl;
    } if(abs(ball_pos_(1) - ball_pos_des_(1)) > epsilon){
        double thetay = -kp_theta_*(ball_pos_des_(1) - ball_pos_(1)) - kv_theta_*(ball_vel_(1));
	thetay = min(0.6,max(-0.6,thetay)); 
        ori_y_des_ << cos(thetay), 0, sin(thetay),
        0, 1, 0,
        -sin(thetay), 0, cos(thetay);
        
                cout << "thetaY " << thetay <<"BALL Velocity                " << ball_vel_.transpose() << endl;
//        cout << "ori_des_" << endl;
//        
//        cout << ori_des_ << endl;
//        cout << "ori_" << endl;
//        
//        cout << ori_ << endl;
        //cout << ball_pos_(0) << " " <<  ball_pos_(1) << endl;
    }

	ori_des_ = ori_x_des_*ori_y_des_;

  //  cout << ball_pos_(0) << " " <<  ball_pos_(1) << endl;
    
    //cout << ball_pos_(0) << " " <<  ball_pos_(1) << endl;
    
       //Jess Code/////////
    
 
    
//    robot->orientationError(delta_phi,ori_des_,ori_);
 //   robot->orientationError(delta_phi,Eigen::Quaterniond(ori_des_),Eigen::Quaterniond(ori_));

    ///advance
    //orientation error
    // object_model->orientationError(delta_phi,o_desired_orientation,Eigen::Quaterniond(o_current_orientation) );
    
    double lam0 ;
    double lam1 ;
    double lam2 ;
    double lam3 ;
    Eigen::MatrixXd  lambda_hat(3,4);
    
    // Convert rotation matrix to Euler parameters and get parameters
    Eigen::Quaterniond lambda(ori_);
    lam0 = lambda.w();
    lam1 = lambda.x();
    lam2 = lambda.y();
    lam3 = lambda.z();
    // calculate lambda_hat matrix
    lambda_hat <<   -lam1,  lam0, -lam3,  lam2,
    -lam2,  lam3,  lam0, -lam1,
    -lam3, -lam2,  lam1,  lam0;
    
    Eigen::VectorXd lambda_des_vec(4);
//    lambda_des_vec<< 1,0,0,0;
    
    Eigen::Quaterniond lambda_des(ori_des_);
    lam0 = lambda_des.w();
    lam1 = lambda_des.x();
    lam2 = lambda_des.y();
    lam3 = lambda_des.z();
    lambda_des_vec<< lam0, lam1, lam2, lam3;
    
    
    delta_phi = (-2 * lambda_hat * lambda_des_vec) ;// -Er_plus * (lambda_des - lambda_current);//
    
    cout <<  "desired" << endl;
    cout <<  ball_pos_des_.transpose() << endl;
    cout <<  "pos" << endl;
    cout <<  ball_pos_.transpose() << endl;
    
    

    
    F_star_=kp_pos_*(x_des_-x_ ) - (kv_pos_ * dx_);
    M_star_= kp_ori_*(-delta_phi)- kv_ori_*omega_;
    F_orientation_<<F_star_, M_star_;
    F_orientation_= Lambda_ori_*F_orientation_;
//
//    

    
    
            // Control torques
           // Eigen::Vector3d F_x = Lambda_x_ * (-kp_pos_ * x_err );
            //Eigen::VectorXd F_posture = robot->_M * ddq;
    
   
    
    
 
       
            command_torques_ = J_task_.transpose()*F_orientation_  + g_ ;
  
            // - convert rotation matrix to quaternion, ensure that quaternion has not flipped sign
            ee_ori_des_ = Eigen::Quaterniond(ori_des_);
            // Send command
            x_des_opp_ << ee_pos_des_, ee_ori_des_.w(), ee_ori_des_.x(), ee_ori_des_.y(), ee_ori_des_.z();
    

  

    
  
    
    
	return RUNNING;
}

/**
 * public MazeProject::initialize()
 * --------------------------------
 * Initialize timer and Redis client
 */
void MazeProject::initialize() {


    // Start redis client
    // Make sure redis-server is running at localhost with default port 6379
    //auto redis_client_ = RedisClient();
    redis_client_.serverIs(kRedisServerInfo);
    

    // Set Ctrl C handler
    setCtrlCHandler(stop);
    g_redis_client_ = &redis_client_;
    
    robot->updateModel();
    ball->updateModel();
    
    
    /***** Hold *****/
    
    cout << "HOLD" << endl;
    
    redis_client_.set(Puma::KEY_CONTROL_MODE, "JHOLD");
    

    // Create a loop timer
    timer_.setLoopFrequency(kControlFreq);   // 1 KHz
    // timer.setThreadHighPriority();  // make timing more accurate. requires running executable as sudo.
    timer_.setCtrlCHandler(stop);    // exit while loop on ctrl-c
    timer_.initializeTimer(kInitializationPause); // 1 ms pause before starting loop
    
    // Set gains in Redis if not initialized
    //if (!redis_client_.getCommandIs(KP_POSITION_KEY)) {
    redis_buf_ = to_string(kp_pos_);
    redis_client_.setCommandIs(KP_POSITION_KEY, redis_buf_);
    //}
    //if (!redis_client_.getCommandIs(KV_POSITION_KEY)) {
    redis_buf_ = to_string(kv_pos_);
    redis_client_.setCommandIs(KV_POSITION_KEY, redis_buf_);
    //}
    //if (!redis_client_.getCommandIs(KP_ORIENTATION_KEY)) {
    redis_buf_ = to_string(kp_ori_);
    redis_client_.setCommandIs(KP_ORIENTATION_KEY, redis_buf_);
    //}
    //if (!redis_client_.getCommandIs(KV_ORIENTATION_KEY)) {
    redis_buf_ = to_string(kv_ori_);
    redis_client_.setCommandIs(KV_ORIENTATION_KEY, redis_buf_);
    //}
    //if (!redis_client_.getCommandIs(KP_JOINT_KEY)) {
    redis_buf_ = to_string(kp_joint_);
    redis_client_.setCommandIs(KP_JOINT_KEY, redis_buf_);
    //}
    //if (!redis_client_.getCommandIs(KV_JOINT_KEY)) {
    redis_buf_ = to_string(kv_joint_);
    redis_client_.setCommandIs(KV_JOINT_KEY, redis_buf_);
    //}
    
    //if (!redis_client_.getCommandIs(KP_JOINT_KEY)) {
    redis_buf_ = to_string(kp_theta_);
    redis_client_.setCommandIs(KP_THETA_KEY, redis_buf_);
    //}
    //if (!redis_client_.getCommandIs(KV_JOINT_KEY)) {
    redis_buf_ = to_string(kv_theta_);
    redis_client_.setCommandIs(KV_THETA_KEY, redis_buf_);
    //}
    
    Kp.fill(kp_joint_);
    Kv.fill(kv_joint_);
    
    //first init of values
    redis_client_.mset({
        {Puma::KEY_COMMAND_DATA, RedisClient::encodeEigenMatrixString(q_des_)},
        {Puma::KEY_KP, RedisClient::encodeEigenMatrixString(Kp)},
        {Puma::KEY_KV, RedisClient::encodeEigenMatrixString(Kv)}
    });
    
    //set up ball
    redis_client_.setEigenMatrixDerivedString(BALL_JOINT_ANGLES_KEY, ball_q_des_);
    redis_client_.setEigenMatrixDerivedString(BALL_JOINT_VELOCITIES_KEY, ball_dq_des_);
    //for Ball
//    redis_client_.setEigenMatrixDerivedString(BALL_POS_DES_KEY, ball_pos_des_);
//    
//    redis_client_.setEigenMatrixDerivedString(BALL_POS_KEY, ball_pos_);
//    redis_client_.setEigenMatrixDerivedString(BALL_VEL_KEY, ball_vel_);
    redis_buf_ = to_string(ball_x_pos_);
    redis_client_.setCommandIs(BALL_POS_X_KEY, redis_buf_);
    redis_buf_ = to_string(ball_y_pos_);
    redis_client_.setCommandIs(BALL_POS_Y_KEY, redis_buf_);
    redis_buf_ = to_string(ball_x_pos_des_);
    redis_client_.setCommandIs(BALL_POS_DES_X_KEY, redis_buf_);
    redis_buf_ = to_string(ball_y_pos_des_);
    redis_client_.setCommandIs(BALL_POS_DES_Y_KEY, redis_buf_);

    redis_buf_ = to_string(epsilon);
    redis_client_.setCommandIs(BALL_EPSILON_KEY, redis_buf_);
   redis_buf_ = to_string(ball_frac_);
    redis_client_.setCommandIs(BALL_FRAC_KEY, redis_buf_);
    
    
    redis_client_.setEigenMatrixDerivedString(BALL_JOINT_TORQUES_COMMANDED_KEY, ball_command_torques_);
    
	cout << "Updating Robots" << endl;
    
    
}

/**
 * public MazeProject::runLoop()
 * -----------------------------
 * MazeProject state machine
 */



void MazeProject::runLoop() {
	
	while (g_runloop) {

        // Wait for next scheduled loop (controller must run at precise rate)
        timer_.waitForNextLoop();
        ++controller_counter_;
        
        // Get latest sensor values from Redis and update robot model
        readRedisValues();
        updateModel();

		switch (controller_state_) {
			// Wait until valid sensor values have been published to Redis
			case REDIS_SYNCHRONIZATION:
                
                
                
				if (isnan(robot->_q) ) {
                    cout << "Sync" << endl;
                };
                if(!isnan(robot->_q)){
				cout << "Redis synchronized. Switching to joint space controller." << endl;
				controller_state_ = JOINT_SPACE_INITIALIZATION;
                
                cout << "JGOTO" << endl;
                };
                
            
                
				break;

			// Initialize robot to default joint configuration
			case JOINT_SPACE_INITIALIZATION:
                
				if (computeJointSpaceControlTorques() == FINISHED) {
                    
					cout << "Joint position initialized. Switching to operational space controller." << endl;
                    cout << "GOTO" << endl;
                  
                        //set up ball
                        redis_client_.setEigenMatrixDerivedString(BALL_JOINT_ANGLES_KEY, ball_q_des_);
                        redis_client_.setEigenMatrixDerivedString(BALL_JOINT_VELOCITIES_KEY, ball_dq_des_);
                        //for Ball
                        redis_client_.setEigenMatrixDerivedString(BALL_JOINT_TORQUES_COMMANDED_KEY, ball_command_torques_);
//                            ball->_q = ball_q_des_;
//                            ball->_dq = ball_dq_des_;
                        ball->updateModel();

			ee_pos_init_ = x_;
                    
                        controller_state_ = OP_SPACE_POSITION_CONTROL;
				};
              

               

				break;

			// Control end effector to desired position
			case OP_SPACE_POSITION_CONTROL:
				computeOperationalSpaceControlTorques();
        
              
				break;

			// Invalid state. Zero torques and exit program.
			default:
				cout << "Invalid controller state. Stopping controller." << endl;
				g_runloop = false;
				command_torques_.setZero();
                ball_command_torques_.setZero();
				break;
		}

		// Check command torques before sending them
		if (isnan(command_torques_)) {
			cout << "NaN command torques. Sending zero torques to robot." << endl;
			command_torques_.setZero();
            ball_command_torques_.setZero();
		}

		// Send command torques
		writeRedisValues();
	}

    
        // Zero out torques before quitting
        command_torques_.setZero();
        ball_command_torques_.setZero();
        redis_client_.setEigenMatrixDerivedString(JOINT_TORQUES_COMMANDED_KEY, command_torques_);
        redis_client_.setEigenMatrixDerivedString(BALL_JOINT_TORQUES_COMMANDED_KEY, ball_command_torques_);
  
        //stop robot
        redis_client_.set(Puma::KEY_CONTROL_MODE, "BREAK");
    

}

int main(int argc, char** argv) {

	// Parse command line
	if (argc != 6) {
		cout << "Usage: maze_project <path-to-world.urdf> <path-to-robot.urdf> <robot-name> <path-to-ball.urdf> <ball_name>" << endl;
		exit(0);
	}
	// argument 0: executable name
	// argument 1: <path-to-world.urdf>
	world_file=(argv[1]);
	// argument 2: <path-to-robot.urdf>
	robot_file=(argv[2]);
	// argument 3: <robot-name>
	robot_name=(argv[3]);
    // argument 2: <path-to-ball.urdf>
    ball_file=(argv[4]);
    // argument 3: <ball-name>
    ball_name=(argv[5]);
    
    // Load robot
    cout << "Loading robot for Maze: " << robot_file << endl;
    auto robot = make_shared<Model::ModelInterface>(robot_file, Model::rbdl, Model::urdf, false);
    
    //Load Ball
    cout << "Loading ball: " << ball_file << endl;
    auto ball = make_shared<Model::ModelInterface>(ball_file, Model::rbdl, Model::urdf, false);
   	// Start controller app
    cout << "Initializing app with " << robot_name << endl;
    
    MazeProject app(move(robot), robot_name, move(ball), ball_name);
    

	app.initialize();
   // initialize();
	cout << "App initialized. Waiting for Redis synchronization." << endl;

  
    
 


	app.runLoop();


	return 0;
}


