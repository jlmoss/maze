#ifndef MAZE_PROJECT_H
#define MAZE_PROJECT_H

// cs225a includes
#include "redis/RedisClient.h"
#include "timer/LoopTimer.h"

// external includes
#include <Eigen/Core>
#include <hiredis/hiredis.h>
#include <model/ModelInterface.h>

// std includes
#include <string>
#include <thread>

class MazeProject {

public:

	MazeProject(std::shared_ptr<Model::ModelInterface> robot,
                const std::string &robot_name,std::shared_ptr<Model::ModelInterface> ball,
                const std::string &ball_name) :
    
        ball(ball),
        ball_dof(ball->dof()),
        BALL_JOINT_TORQUES_COMMANDED_KEY(kRedisKeyPrefix + ball_name + "::actuators::fgc"),
        BALL_JOINT_ANGLES_KEY           (kRedisKeyPrefix + ball_name + "::sensors::q"),
        BALL_JOINT_VELOCITIES_KEY       (kRedisKeyPrefix + ball_name + "::sensors::dq"),
    
//    BALL_POS_DES_KEY			(kRedisKeyPrefix + ball_name + "::sensors::ball_pos_des"),
//    BALL_POS_KEY				(kRedisKeyPrefix + ball_name + "::sensors::ball_pos"),
//    BALL_VEL_KEY				(kRedisKeyPrefix + ball_name + "::sensors::ball_vel"),
    
    BALL_EPSILON_KEY				(kRedisKeyPrefix +"maze::epsilon"),
    BALL_FRAC_KEY				(kRedisKeyPrefix +"maze::frac"),
    BALL_POS_X_KEY				(kRedisKeyPrefix +"maze::x"),
    BALL_POS_Y_KEY				(kRedisKeyPrefix +"maze::y"),
    BALL_POS_DES_X_KEY			(kRedisKeyPrefix +"maze::tx"),//desired postion
    BALL_POS_DES_Y_KEY			(kRedisKeyPrefix +"maze::ty"),//desired postion
    BALL_MASS_KEY				(kRedisKeyPrefix +"maze::mass"),//number of points for detecting the ball

    
        ball_command_torques_(ball_dof),
        ball_q_des_(ball_dof),
        ball_dq_des_(ball_dof),
 

        ball_pos_(3),
        ball_vel_(3),
    
		robot(robot),
		dof(robot->dof()),
		JOINT_TORQUES_COMMANDED_KEY(kRedisKeyPrefix + robot_name + "::actuators::fgc"),
		EE_POSITION_KEY            (kRedisKeyPrefix + robot_name + "::tasks::ee_pos"),
		EE_POSITION_DESIRED_KEY    (kRedisKeyPrefix + robot_name + "::tasks::ee_pos_des"),
		JOINT_ANGLES_KEY           (kRedisKeyPrefix + robot_name + "::sensors::q"),
		JOINT_VELOCITIES_KEY       (kRedisKeyPrefix + robot_name + "::sensors::dq"),
		TIMESTAMP_KEY              (kRedisKeyPrefix + robot_name + "::timestamp"),
		KP_POSITION_KEY            (kRedisKeyPrefix + robot_name + "::tasks::kp_pos"),
		KV_POSITION_KEY            (kRedisKeyPrefix + robot_name + "::tasks::kv_pos"),
		KP_ORIENTATION_KEY         (kRedisKeyPrefix + robot_name + "::tasks::kp_ori"),
		KV_ORIENTATION_KEY         (kRedisKeyPrefix + robot_name + "::tasks::kv_ori"),
		KP_JOINT_KEY               (kRedisKeyPrefix + robot_name + "::tasks::kp_joint"),
		KV_JOINT_KEY               (kRedisKeyPrefix + robot_name + "::tasks::kv_joint"),
        KP_THETA_KEY               (kRedisKeyPrefix + robot_name + "::tasks::kp_theta"),
        KV_THETA_KEY               (kRedisKeyPrefix + robot_name + "::tasks::kv_theta"),
		command_torques_(dof),
		Jv_(3, dof),
		N_(dof, dof),
		Lambda_x_(3, 3),
        J_task_(6, 6),
        Lambda_ori_(6, 6),
        F_orientation_(6),
		g_(dof),
		q_des_(dof),
		dq_des_(dof),
		controller_state_(REDIS_SYNCHRONIZATION)
    
  
    
	{
		command_torques_.setZero();
        ball_command_torques_.setZero();

		// Home configuration for Puma
		q_des_ << -2.341768412883, 0.606386467035, 0.326739705453, -0.042901893975, -0.88609181816, 2.398218905878;
		//q_des_ *= M_PI / 180.0;
		dq_des_.setZero();
        //set ball home  //0.225 -1.18 -0.425"
          ball_q_des_ <<  0, 0, 0;
          ball_q_des_ *= M_PI / 180.0;
          ball_dq_des_.setZero();
        ball_pos_des_.setZero();
ball_pos_last_.setZero();
        ball_vel_.setZero();
      

		// Desired end effector position
		x_des_ << 0.43, 0.23, 0.3;
		dx_des_.setZero();
        
        ee_pos_init_ <<  0.45, 0.27, 0.16;
        ee_pos_des_ = ee_pos_init_;
       // ee_ori_des_ << 1.0, 0.0, 0.0, 0.0;
        
        
	}

	/***** Public functions *****/

	void initialize();
	void runLoop();

protected:

	/***** Enums *****/

	// State enum for controller state machine inside runloop()
	enum ControllerState {
		REDIS_SYNCHRONIZATION,
		JOINT_SPACE_INITIALIZATION,
		OP_SPACE_POSITION_CONTROL
	};

	// Return values from computeControlTorques() methods
	enum ControllerStatus {
		RUNNING,  // Not yet converged to goal position
		FINISHED  // Converged to goal position
	};

	/***** Constants *****/

	const int dof, ball_dof;  // Initialized with robot model
	const double kToleranceInitQ  = .15;  // Joint space initialization tolerance
	const double kToleranceInitDq = .15;  // Joint space initialization tolerance
	const double kMaxVelocity = 0.5;  // Maximum end effector velocity

	const int kControlFreq = 1000;         // 1 kHz control loop
	const int kInitializationPause = 1e6;  // 1ms pause before starting control loop

	const HiredisServerInfo kRedisServerInfo = {
		"127.0.0.1",  // hostname
		6379,         // port
		{ 1, 500000 } // timeout = 1.5 seconds
	};

	// Redis keys:
	const std::string kRedisKeyPrefix = "cs225a::robot::";
	// - write:
	const std::string JOINT_TORQUES_COMMANDED_KEY;
	const std::string EE_POSITION_KEY;
	const std::string EE_POSITION_DESIRED_KEY;
//    const std::string BALL_POS_DES_KEY;
//    const std::string BALL_VEL_DES_KEY;
//    const std::string BALL_POS_KEY;4
//    const std::string BALL_VEL_KEY;
	// - read:
	const std::string JOINT_ANGLES_KEY;
	const std::string JOINT_VELOCITIES_KEY;
	const std::string TIMESTAMP_KEY;
	const std::string KP_POSITION_KEY;
	const std::string KV_POSITION_KEY;
	const std::string KP_ORIENTATION_KEY;
	const std::string KV_ORIENTATION_KEY;
	const std::string KP_JOINT_KEY;
	const std::string KV_JOINT_KEY;
    const std::string KP_THETA_KEY;
    const std::string KV_THETA_KEY;
	const std::string KP_JOINT_INIT_KEY;
	const std::string KV_JOINT_INIT_KEY;
      
    
  //from Camera
    const std::string BALL_POS_DES_X_KEY;
    const std::string BALL_POS_DES_Y_KEY;
    const std::string BALL_POS_X_KEY;
    const std::string BALL_POS_Y_KEY;
    const std::string BALL_MASS_KEY;
	const std::string  BALL_EPSILON_KEY;
const std::string  BALL_FRAC_KEY;
    
    //for ball
    // - read:
    const std::string BALL_JOINT_TORQUES_COMMANDED_KEY;
    // - write:
    const std::string BALL_JOINT_ANGLES_KEY;
    const std::string BALL_JOINT_VELOCITIES_KEY;
  

	/***** Member functions *****/

	void readRedisValues();
	void updateModel();
	void writeRedisValues();
	ControllerStatus computeJointSpaceControlTorques();
	ControllerStatus computeOperationalSpaceControlTorques();

	/***** Member variables *****/

	// Robot
	const std::shared_ptr<Model::ModelInterface> robot;
    //for ball
    const std::shared_ptr<Model::ModelInterface> ball;
    
	// Redis
	RedisClient redis_client_;
	std::string redis_buf_;

	// Timer
	LoopTimer timer_;
	double t_curr_;
double t_last_;
	uint64_t controller_counter_ = 0;

	// State machine
	ControllerState controller_state_;

	// Controller variables
	Eigen::VectorXd command_torques_ , ball_command_torques_;
	Eigen::MatrixXd Jv_;
    Eigen::MatrixXd Jw_;
    Eigen::MatrixXd J_task_ ;
	Eigen::MatrixXd N_;
	Eigen::MatrixXd Lambda_x_;
    Eigen::MatrixXd Lambda_ori_;
	Eigen::VectorXd g_;
	Eigen::Vector3d x_, dx_;
	Eigen::VectorXd q_des_, dq_des_;
	Eigen::Vector3d x_des_, dx_des_;
    Eigen::Vector3d ball_pos_;
   Eigen::Vector3d ball_pos_last_;

    Eigen::Vector3d ball_pos_des_;
    Eigen::Vector3d ball_vel_;
    
    Eigen::Vector3d delta_phi;
    Eigen::Matrix3d ori_des_;
  Eigen::Matrix3d ori_x_des_;
  Eigen::Matrix3d ori_y_des_;
    Eigen::Matrix3d ori_;
    Eigen::Vector3d omega_;
    
    
    Eigen::Vector3d ee_pos_init_ ;
    Eigen::Vector3d ee_pos_des_ ;
    Eigen::Quaterniond ee_ori_des_;
    
    Eigen::Vector3d F_star_;
    Eigen::Vector3d M_star_;
    Eigen::VectorXd F_orientation_;
    

    
    
    //ball variables
    Eigen::Vector3d ball_x_, ball_dx_;
    Eigen::VectorXd ball_q_des_, ball_dq_des_;

    const double kAmplitude = 0.1;
    //set initial ball to zero
    double ball_x_pos_ = 0.0 ;
    double ball_y_pos_ = 0.0 ;

    double ball_x_pos_des_ = 0.0 ;
    double ball_y_pos_des_ = 0.0 ;
    double epsilon = 0.004;
       double ball_frac_ = 0.01;
	// Default gains (used only when keys are nonexistent in Redis)
	double kp_pos_ = 100;
	double kv_pos_ = 40;
	double kp_ori_ = 100;
	double kv_ori_ = 60;
	double kp_joint_ = 230;
	double kv_joint_ = 40;
    double kp_theta_ = -3;
    double kv_theta_ =0;
};

#endif //MAZE_PROJECT_H
